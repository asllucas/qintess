package br.com.web.api.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.web.api.models.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> { }
